package info.bitcoinunlimited.www.wally

import android.content.Context
import okio.FileSystem
import okio.Path.Companion.toPath
import okio.buffer
import okio.source
import org.nexa.libnexakotlin.GroupId
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

object AndroidAssetManagerStorage:AssetManagerStorage
{
    override fun storeAssetFile(filename: String, data: ByteArray): String
    {
        val context = wallyAndroidApp

        val dir = context!!.getDir("asset", Context.MODE_PRIVATE)
        val file = File(dir, filename)
        FileOutputStream(file).use {
            it.write(data)
        }
        return file.absolutePath
    }
    override fun loadAssetFile(filename: String): Pair<String, EfficientFile>
    {
        if (DBG_NO_ASSET_CACHE) throw Exception()
        val context = wallyAndroidApp
        val dir = context!!.getDir("asset", Context.MODE_PRIVATE)
        val file = File(dir, filename)
        val name = file.absolutePath
        val ef = EfficientFile(name.toPath(), FileSystem.SYSTEM)
        return Pair(name, ef)
    }

    /** delete a particular asset file */
    override fun deleteAssetFile(filename: String)
    {
        val context = wallyAndroidApp
        val dir = context!!.getDir("asset", Context.MODE_PRIVATE)
        val file = File(dir, filename)
        file.delete()
    }

    /** delete all asset files */
    override fun deleteAssetFiles()
    {
        val context = wallyAndroidApp
        val dir = context!!.getDir("asset", Context.MODE_PRIVATE)
        val files = dir.listFiles()
        for (f in files)
            f.delete()
    }

    override fun storeCardFile(filename: String, data: ByteArray): String
    {
        val context = wallyAndroidApp

        context!!.openFileOutput(filename, Context.MODE_PRIVATE).use {
            it.write(data)
        }
        val path =  context.filesDir.toString() + File.separator + filename
        return path
    }

    override fun loadCardFile(filename: String): Pair<String, ByteArray>
    {
        if (DBG_NO_ASSET_CACHE) throw Exception()
        val localname = if (filename.startsWith("file://")) filename.drop(7) else filename
        val file = File(localname)
        val name = file.absolutePath
        FileInputStream(file).use {
            return Pair(name, it.readBytes())
        }
    }

    override fun cacheNftMedia(groupId: GroupId, media: Pair<String?, ByteArray?>): Pair<String?, ByteArray?>
    {
        val cacheDir = wallyAndroidApp!!.cacheDir
        var uriStr = media.first
        var b = media.second
        if (b != null)
        {
            // Choose to always cache the file regardless of whether we also hold onto its bytes
            if (true) // if ((b.size > MAX_UNCACHED_FILE_SIZE) || (uriStr!=null && isVideo(uriStr)))
            {
                val result = canonicalSplitExtension(uriStr)
                if (result == null) return Pair(uriStr, b)  // never going to happen because uriStr != null
                val (fnoext, ext) = result
                File.createTempFile(groupId.toStringNoPrefix() + "_" + fnoext, ext, cacheDir)
                val f = File(cacheDir, groupId.toStringNoPrefix() + "_" + fnoext + "." + ext)
                uriStr = f.absolutePath
                f.writeBytes(b)
                if (b.size > MAX_UNCACHED_FILE_SIZE) b = null  // We want to load this from cache file so don't save the bytes
            }
        }
        return Pair(uriStr, b)
    }
}


actual fun assetManagerStorage(): AssetManagerStorage = AndroidAssetManagerStorage