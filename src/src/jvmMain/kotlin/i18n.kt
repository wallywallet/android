package info.bitcoinunlimited.www.wally

import okio.BufferedSource
import okio.Okio
import okio.buffer
import okio.source
import org.nexa.libnexakotlin.decodeUtf8
import org.nexa.libnexakotlin.Objectify
import java.io.BufferedInputStream
import java.io.File
import java.io.InputStream
import java.util.*


var LocaleStrings = arrayOf<String>()

/** Convert this number to a locale-based string */
actual fun i18n(id: Int): String
{
    if (id > LocaleStrings.size - 1) return("STR$id")
    val s = LocaleStrings[id]
    return s
}


actual fun setLocale():Boolean
{
    val locale = Locale.getDefault()
    return setLocale(locale.language, locale.country)
}
actual fun setLocale(language: String, country: String, context: Any?):Boolean
{
    val nothing = Objectify<Int>(0)

    val loadTries = listOf<()->ByteArray?> (
      { nothing::class.java.getClassLoader().getResourceAsStream("strings_${language}_${country}.bin")?.readBytes() },
      { nothing::class.java.getClassLoader().getResourceAsStream("strings_${language}.bin")?.readBytes() },
      { File("strings_${language}_${country}.bin").readBytes() },
      { File("strings_${language}.bin").readBytes() }
      )

    var strs = byteArrayOf()
    for (i in loadTries)
    {
        try
        {
            strs = i() ?: continue
            break
        }
        catch (_:Exception)
        {}
    }
    if (strs.size == 0) return false
    val chopSpots = mutableListOf<Int>(0)
    strs.forEachIndexed { index, byte -> if (byte==0.toByte()) chopSpots.add(index+1)  }

    val strings = Array<String>(chopSpots.size-1, { i:Int->
        val ba = strs.sliceArray(chopSpots[i] until chopSpots[i+1]-1)
        val s = ba.decodeUtf8().replace("\\n","\n").replace("\\'","\'").replace("\\\"","\"")
        s
    })
    LocaleStrings = strings
    return true
}