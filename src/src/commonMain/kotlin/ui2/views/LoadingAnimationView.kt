package info.bitcoinunlimited.www.wally.ui2.views

import androidx.compose.runtime.Composable

var loadingAnimation:String? = null
@Composable
expect fun LoadingAnimationContent()
