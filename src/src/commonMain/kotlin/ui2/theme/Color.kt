package info.bitcoinunlimited.www.wally.ui2.theme

import androidx.compose.ui.graphics.Color

val colorPrimary = Color(0xFFD0A6FF)
val colorPrimaryDark = Color(0xFF5B276B)
val colorDefault = Color(0xFF808080)  // gray
val splashBackground = Color(0xFF5B276B)
val colorAccent = Color(0xFFC01BD0)

val defaultSpinnerBkg = Color(0xFFffffffff)
val defaultSpinnerBorder = Color(0xFFefddff)
val softKeyboardTop = Color(0xFFFFF6F6F6)
val defaultListHighlight = Color(0xFFECE1B6)
val defaultListHighlight2 = Color(0xFFe3d395)

val colorDebit = Color(0xFFB43200)
val colorCredit = Color(0xFF70A01E)
val colorConfirm = Color(0xFFE48864)

val colorValid = Color(0xFF70A01E)
val colorException = Color(0xFFFF5080)
val colorError = Color(0xFFCF6050)
val colorNotice = Color(0xFF408040)
val colorWarning = Color(0xFFFFD040)
val colorTitleBackground = Color(0xFF725092)
val colorTitleForeground = Color.White

val rowA = Color(0xFFFFEFEFEF)
val rowB = Color(0xFFFFC0C0C0)

val WallyRowAbkg1 = Color(0xFF8Ff0f0ff)
val WallyRowBbkg1 = Color(0xFF8FF4e0ec)
val WallyRowAbkg2 = Color(0xFFBFfff5f0)
val WallyRowBbkg2 = Color(0xFFBFf5f5ff)

var WallyShoppingRowColors = arrayOf(Color(0x4Ff5f8ff), Color(0x4Fd0d0ef))

val black = Color(0xFFFF000000)
val white = Color(0xFFFFffffff)
val WallyAddressColor = Color(0xB0707070)
val WallyButtonBackground = Color(0xFFFFDEDEDE)
val WallyButtonBackgroundDisabled = Color(0xFFFF9E9E9E)
val WallyBoringButtonShadow = Color(0xFFFF6E6E6E)
val WallyButtonForeground = black
val WallyBorder = Color(0xFFFFefddff)
val BaseBkg = Color(0xFFFFFCF0D9)
val BrightBkg = Color(0xFFFFFFFAF0)
val SelectedBkg = Color(0xFFe3d8c3) //Color(0xFFFF8F8A80)  // 10% blacker than BaseBkg
val NavBarBkg = Color(0xFFFFF5EBC8)
val ModalBkg = BrightBkg
val ModalItemBkg = BrightBkg

val listDividerFg = Color(0xFFFF906EB0)
val unconfirmedBalanceIncomingColor = Color(0xFFFFa19120)
val unconfirmedBalanceOutgoingColor = Color(0xFFFFBD4311)
val unsyncedBalanceColor = Color(0xFFFF9ac3e9)
val unsyncedStatusColor = Color(0xFFFF80a0FF)
val accountSelectionBkg = white
val accountSelectionBorder = WallyBorder

val nuggetBody = Color(0xFFFFdfbc00)
val nuggetChip = Color(0xFFFFFF9b8d44)

val floatingActionBarBackground = Color(0xD0735092)