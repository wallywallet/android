// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.www.wally

import org.nexa.libnexakotlin.*
import com.ionspin.kotlin.bignum.decimal.*
import io.ktor.client.*
import io.ktor.http.*
import io.ktor.client.request.*
import io.ktor.client.statement.*

import kotlinx.coroutines.*
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.Serializable

//import java.net.URL

import kotlin.time.*
import kotlin.time.TimeSource.Monotonic

private val LogIt = GetLog("BU.wally.bitcoincom")

val POLL_INTERVAL = 30000

@Serializable
data class BchUsdBitcoinCom(val price: Int, val stamp: Long)


@Serializable
data class HistItemBitcoinCom(val price: Int)

@Serializable
data class HistBitcoinCom(val lookup: HistItemBitcoinCom)

@OptIn(ExperimentalTime::class)
val lastPoll = mutableMapOf<String, Pair<TimeMark, BigDecimal>>()

@OptIn(ExperimentalTime::class)
fun UbchInFiat(fiat: String, setter: (BigDecimal) -> Unit)
{
    if (!allowAccessPriceData) return
    val prior = lastPoll[fiat]
    if (prior != null)
    {
        if (prior.first.elapsedNow().inWholeMilliseconds < POLL_INTERVAL)
        {
            setter(prior.second)
            return
        }
    }

    val client = HttpClient()

    // TODO periodic update
    launch {
        val data = try
        {
            val u = Url("https://index-api.bitcoin.com/api/v0/cash/price/" + fiat)  //.readText()
            client.get(u)
        }
        catch (e: Exception)
        {
            LogIt.info("Error retrieving price: " + e.message)
            return@launch
        }
        LogIt.info(sourceLoc() + " " + data)
        val parser: Json = Json { isLenient = true; ignoreUnknownKeys = true }  // nonstrict mode ignores extra fields
        val obj = parser.decodeFromString(BchUsdBitcoinCom.serializer(), data.bodyAsText())
        LogIt.info(sourceLoc() + " " + obj.toString())
        // TODO verify recent timestamp
        val v = CurrencyDecimal(obj.price.toLong()) / CurrencyDecimal(100000000) // bitcoin.com price is in cents per BCH.  We want "dollars" per uBCH (millionths of a BCH)
        lastPoll[fiat] = Pair(Monotonic.markNow(), v)
        setter(v)
    }

}

/** Return the approximate price of mBCH at the time provided in seconds since the epoch */
fun historicalUbchInFiat(fiat: String, timeStamp: Long): BigDecimal
{
    if (!allowAccessPriceData) return CurrencyDecimal(-1L)
    if (fiat != "USD") return BigDecimal.ZERO  // TODO get other fiat historical prices

    // see https://index.bitcoin.com/
    val spec = "https://index-api.bitcoin.com/api/v0/cash/lookup?time=" + timeStamp.toString()
    val client = HttpClient()
    val data = runBlocking {
        try
        {
            client.get(spec).bodyAsText()
        }
        catch (e: Exception)
        {
            "-1"
        }
    }
    val parser: Json = Json { isLenient = true; ignoreUnknownKeys = true }  // nonstrict mode and ignore extra fields

    LogIt.info(sourceLoc() + " " + data)

    val obj = parser.decodeFromString(HistBitcoinCom.serializer(), data)
    LogIt.info(sourceLoc() + " " + obj.toString())

    // TODO verify timestamp
    val v = CurrencyDecimal(obj.lookup.price.toLong()) / CurrencyDecimal(100000000) // bitcoin.com price is in cents per BCH.  We want "dollars" per uBCH (millionths of a BCH)
    return v
}